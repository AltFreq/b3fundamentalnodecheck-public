    function check_node(){
         $("#results").show();
            $("#results").html("<center><h2>Scanning...</h2></center>");
            var input = $('#input').val();
        $.ajax({
			type: "GET",
			url: "main.php",
			data: {
				func: "getNodeData",
				node_id: input
			},
			success: function(text) {
                $("#results").html("");
				console.log(text);
                data = JSON.parse(text);
                result = data['result'];
                message = data['message'];
                console.log(result);
                if(!result && message=='error_ip'){
                    $('#error').show();
                    $('#error').text("Invalid IP");
                    return;
                }else{
                    $('#error').hide();
                }

                $("#results").append("<center><h2> Details for FN at "+data["node"]+"</h2></center><br>");
                $("#results").append("<ul>");
                $port_html = "<li><b>Port:"+data["port"]+" - ";
                if(result==false && message=='error_port'){
                    $port_html+="<span style='color:red'>Closed</span></b></li>";
                    $("#results").append($port_html);
//                    'Cannot contact server'
                    return;
                }else{
                    $port_html+="<span style='color:green'>Open</span></b></li>";
                    $("#results").append($port_html);
                }
                if(result==false && message=='error_offline'){
                    $("#results").append("<h2 style='color:red'> Unfortunatly the monitoring wallet is offline</h2><p>Please contact AltFreq#7076 on discord and let him know!</p>");
//                    Wallet is offline
                    return;
                }
                $connection_html = "<li><b>Accepting connections - ";
                if(result==false && message=='error_connection'){
                    $connection_html+="<span style='color:red'> False</span></b></li>";
                    $("#results").append($connection_html);
//                    Cannot make a connection to the actual server
                    return
                }else{
                    $connection_html+="<span style='color:green'> True</span></b></li>";
                    $("#results").append($connection_html);
                }
                $pos_string = "<li><b>Proof-of-Service(POS) score: ";
                $pos_score = parseInt(Object.values(data['pose'])[0]);
                if($pos_score>=6){
                    $pos_string+="<span style='color:red'>"+$pos_score;
                    //POS Error
                    $("#results").append($pos_string);
                    return;
                }else{
                    $pos_string+="<span style='color:green'>"+$pos_score;
                    $("#results").append($pos_string);
                }
                full_data =  Object.values(data['full'])[0].trim().replace(/\s\s+/g, ' ').split(' ');
                status = full_data[0];
//                ENABLED / EXPIRED / VIN_SPENT / REMOVE / POS_ERROR
                status_text = "<li><b>Status - ";
                if(status=='EXPIRED'){
                    status_text+= "<span style='color:red'>"+full_data[0]+"</span></li>";
                    $("#results").append(status_text);
                }else if(status=='VIN_SPENT'){
                    status_text+= "<span style='color:red'>"+full_data[0]+"</span></li>";
                    $("#results").append(status_text);
                }else if(status=='REMOVE'){
                    status_text+= "<span style='color:red'>"+full_data[0]+"</span></li>";
                    $("#results").append(status_text);
                }else if(status=='POS_ERROR'){
                    status_text+= "<span style='color:red'>"+full_data[0]+"</span></li>";
                    $("#results").append(status_text);
                }else{
                    status_text+= "<span style='color:green'>"+full_data[0]+"</span></li>";
                    $("#results").append(status_text);


            //      status protocol pubkey vin lastseen activeseconds
                    protocol_string = "<li><b>Protocol - ";
                    if(parseInt(full_data[1]) < parseInt(data['protocol'])){
                        protocol_string+='<span style="color:red"> '+ full_data[1] + '</span></li>';
                        //wrong wallet version/old
                        $("#results").append(protocol_string);
                        return;
                    }else{
                        protocol_string+='<span style="color:green"> '+ full_data[1] + '</span></li>';
                        $("#results").append(protocol_string);
                    }
                    active_string = "<li><b>Active Seconds - ";
                    if(parseInt(full_data[5]) < 1800){
                        active_string+='<span style="color:red"> '+ full_data[5] + '</span></li>';
                        //new node or something wrong with active seconds
                        $("#results").append(active_string);
                        return;
                    }else{
                        active_string+='<span style="color:green"> '+ full_data[5] + '</span></li>';
                        $("#results").append(active_string);
                    }
                    lastseen_string = "<li><b>Last Seen - ";
                    now = Date.now()/1000;
                    diff = (now - parseInt(full_data[4]))/60
                    if(diff > 30){
                        lastseen_string+='<span style="color:red"> '+ full_data[4] + '</span></li>';
                        //node was last seen over 30 mins ago
                        $("#results").append(lastseen_string);
                        return;
                    }else{
                        lastseen_string+='<span style="color:green"> '+ full_data[4] + '</span></li>';
                        $("#results").append(lastseen_string);
                    }
                    $("#results").append("<h3> Other details:</h3>");
                    $("#results").append("<li><b>Pubkey - " + full_data[2] + '</li>');
                    $("#results").append("<li><b>VIN - " + full_data[3] + '</li>');
                    $("#results").append("</ul>");
                    $("#results").append("<h2>Node appears to be working correctly!</h2>");
                }

			}
		});
}
$( document ).ready(function() {

    $("#submit").click(function(){
       check_node();
    });

    $("body").keypress(function(e){
        if(e.which==13){
            check_node();
        }
    });


    $("#clear").click(function(){
        $("#input").val('');
        $("#results").html('');
        $("#results").hide();
    });
});
