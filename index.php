<!DOCTYPE html>
<?php
    include 'main.php';
?>
<html>
<head>
    <title>B3 Fundamentalnode Checker</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115899398-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-115899398-1');
    </script>
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({
              google_ad_client: "ca-pub-1106988235307541",
              enable_page_level_ads: true
         });
    </script>
</head>
<body>
        <div id='contents'>
        <center>
            <h1>B3 Fundamentalnode Checker</h1>
            <h3> Total FNs currently in database: <?php getTotalFNs() ?>  </h3>
            <input type=text maxlength="64" size="64" placeholder="Paste in your node address" id="input">
            <br>
            <div id="error" style="display:none;">

            </div>
            <br>
            <input type="submit" value="Check" id="submit"> <input type="button" value="Clear" id="clear">
            <br><br>
        </center>
            <div id="results" style="display:none">

            </div>
    </div>
<!-- Java Script
================================================== -->
<script src="jquery-1.11.3.min.js"></script>
<script src="main.js"></script>
</body>
</html>

